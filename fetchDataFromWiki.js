// To get new data, go to https://enterthegungeon.fandom.com/wiki/Guns and paste this in the console, then type 'gunData' and copy the object.

let headers = [
  'icon',
  'name',
  'quote',
  'quality',
  'type',
  'dps',
  'magSize',
  'ammoCapacity',
  'damage',
  'fireRate',
  'reloadTime',
  'shotSpeed',
  'range',
  'force',
  'spread',
  'notes',
];

const tableBody = document.querySelector('table.wikitable tbody');

let gunRows = Array.from(tableBody.children).map((child) =>
  Array.from(child.children)
);

let gunData = gunRows.map((row) => {
  let data = {};
  const matchNAOrFirstWordOfQuality = /(N\/A)|^\S+/g;

  for (let i = 0; i < row.length; i++) {
    switch (i) {
      case 0:
        data['icon'] = row[0].children[0].children[0].getAttribute('src');
        break;
      case 1:
        data['name'] = row[1].textContent.trim();
        data['wikiLink'] = row[1].children[0].getAttribute('href');
        break;
      case 3:
        data['quality'] = row[3].children[0].children[0]
          .getAttribute('alt')
          .match(matchNAOrFirstWordOfQuality)[0]
          .replace('1S', 'S');
        break;
      default:
        data[headers[i]] = row[i].textContent.trim();
    }
  }

  return data;
});
